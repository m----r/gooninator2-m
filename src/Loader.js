import getRandomListItem from "./getRandomListItem";
 export default class Loader {
  constructor() {
    this.queue = [];
    this.consumerIndex = 0;
    this.isStopped = true;
    this.loadedImageURLs = {};
    this.sourcesByImageSrc = {};

    this.lastConfigJSON = "";
  }

  configure(fetchers, numConcurrent, maxCount, includeGifs, includeStills, minImageWidth, displayCountUpdateCallback) {
    const newConfigJSON = JSON.stringify({
      sources: fetchers.map((f) => f.source),
      includeGifs,
      includeStills,
      minImageWidth,
    });
    if (this.lastConfigJSON === newConfigJSON) return;
    this.lastConfigJSON = newConfigJSON;

    // console.log("Resetting loader");

    this.fetchers = fetchers;
    this.maxCount = maxCount;
    this.numConcurrent = numConcurrent;
    this.includeGifs = includeGifs;
    this.includeStills = includeStills;
    this.minImageWidth = minImageWidth;
    this.displayCountUpdateCallback = displayCountUpdateCallback;

    this.queue = [];
    this.consumerIndex = 0;
    this.loadedImageURLs = {};
  }

  start() {
    if (!this.isStopped) return;

    this.isStopped = false;
    for (let i=0; i < this.numConcurrent; i++) {
      this.startFetchImageLoop(i);
    }
  }

  stop() {
    this.isStopped = true;
  }

  getNext() {
    if (!this.queue.length) return {image: null, source: null};

    let image = null;
    if (this.consumerIndex >= this.queue.length) {
      image = getRandomListItem(this.queue);
    } else {
      image = this.queue[this.consumerIndex];
      this.consumerIndex += 1;
    }

    if (this.queue.length > this.maxCount) {
      this.deleteFirstImage();
    }

    return {image, source: this.sourcesByImageSrc[image.src]};
  }

  deleteFirstImage() {
    const img = this.queue.shift();
    delete this.loadedImageURLs[img.src]
    this.consumerIndex = Math.max(this.consumerIndex - 1, 0);
    this.displayCountUpdateCallback(this.queue.length);
  }

  startFetchImageLoop(i) {
    // console.log("Starting process", i);
    const fetchOneImage = () => {
      if (this.isStopped) {
        // console.log("Stopping process", i);
        return;
      }

      // The image display loop consumes the queue, so just wait for it to eat
      // enough images
      if (this.queue.length > this.maxCount) {
        // console.log("Waiting to outrun queue");
        return setTimeout(fetchOneImage, 100);
      }

      const fetcher = getRandomListItem(this.fetchers);
      let myUrls = fetcher.allURLs;
      if (!this.includeGifs) myUrls = fetcher.stillURLs;
      if (!this.includeStills) myUrls = fetcher.gifURLs;

      const img = new Image()
      const url = getRandomListItem(myUrls);

      if (!url) {
        // console.log("Nothing loaded yet; waiting");
        return setTimeout(fetchOneImage, 100);
      }

      if (this.loadedImageURLs[url]) {
        // console.log("Skipping duplicate");
        // already loaded; try again but limit recursion
        return setTimeout(fetchOneImage, 0);
      }

      if (fetcher.sizesByURL[url].width < this.minImageWidth) {
        // console.log("Skipping too small image");
        return setTimeout(fetchOneImage, 0);
      }

      img.onload = () => {
        // console.log("Loaded", img.src);
        img.fetcher = fetcher;
        this.queue.push(img);
        this.sourcesByImageSrc[img.src] = fetcher.source;
        this.displayCountUpdateCallback(this.queue.length);
        fetchOneImage();
      };

      img.onerror = () => {
        // console.error(img.src);
        fetchOneImage();
        delete this.loadedImageURLs[img.src];
      };

      this.loadedImageURLs[img.src] = true;
      img.src = url;
      // console.log("Loading", img.src);
    }
    fetchOneImage();
  }
}