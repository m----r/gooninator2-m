import React, { Component, PropTypes } from 'react';
import store from "./Store";
import getRandomListItem from "./getRandomListItem";


class Subdirectory extends Component {
  static propTypes = {
    category: PropTypes.object.isRequired,
    sources: PropTypes.array.isRequired,
    favorites: PropTypes.object.isRequired,
  };

  render() {
    const items = this.props.category.items.filter((source) => {
      return this.props.sources.indexOf(source) === -1;
    });
    return (
      <div className="gn-subdirectory fill-container">
        {Boolean(items.length) && (
            <div onClick={store.pusher('sources', getRandomListItem(items))}
                 key={"gn-random"}
                 className="gn-form-entry gn-directory-entry">
              Random
            </div>
        )}
        {items.map((source, i) => {
          return (
            <div onClick={store.pusher('sources', source)}
                 key={source}
                 className="gn-form-entry gn-directory-entry">
              {source} {this.props.favorites[source] && "★"}
            </div>
          );
        })}
      </div>
    );
  }
}


class Directory extends Component {
  static propTypes = {
    directoryCategoryIndex: PropTypes.number,
    favorites: PropTypes.object.isRequired,
    categories: PropTypes.array.isRequired,
  };

  render() {
    const favoritesCategory = {
      label: "Favorites",
      items: Object.keys(this.props.favorites).sort((a, b) => a < b ? -1 : 1),
    };
    console.log(favoritesCategory);

    return (
      <div className="gn-directory fill-container">
        {this.props.categories.map((category, i) => {
          const className = i === this.props.directoryCategoryIndex
            ? "gn-form-entry gn-directory-entry gn-category gn-category-selected"
            : "gn-form-entry gn-directory-entry gn-category"
          return (
            <div onClick={store.setter('directoryCategoryIndex', i)}
                 key={category.label}
                 className={className}>
              {category.label}
            </div>
          );
        })}
      </div>
    );
  }
}


export {Directory, Subdirectory}