import React, {Component, PropTypes} from "react";


export default class ImageDisplay extends Component {
  static propTypes = {
    img: PropTypes.instanceOf(Image),
  };

  componentDidMount() {
    this._applyImage();
  }

  componentDidUpdate(prevProps, prevState) {
    this._applyImage();    
  }

  _applyImage() {
    const el = this.el;
    if (!el) return "No element";

    const img = this.props.img;
    if (!img) return "No image available";

    if (el.firstChild && el.firstChild.src === img.src) return;

    const parentWidth = el.offsetWidth;
    const parentHeight = el.offsetHeight
    const parentAspect = parentWidth / parentHeight;
    const imgAspect = img.width / img.height;

    if (imgAspect < parentAspect) {
      const scale = parentHeight / img.height;
      img.style.width = 'auto';
      img.style.height = '100%';
      img.style.marginTop = 0;
      img.style.marginLeft = (parentWidth / 2 - img.width * scale / 2) + 'px';
    } else {
      const scale = parentWidth / img.width;
      img.style.height = 'auto';
      img.style.width = '100%';
      img.style.marginTop = (parentHeight / 2 - img.height * scale / 2) + 'px';
      img.style.marginLeft = 0;
    }

    if (el.firstChild) {
      el.removeChild(el.firstChild);
    }
    el.appendChild(img);
  }

  render() {
    return <div className="fill-container" ref={(el) => { this.el = el }} />;
  }
}
