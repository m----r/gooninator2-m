import React, { Component } from 'react';
import './normalize.css';
import './App.css';
import store from "./Store";
import SourceForm from "./SourceForm";
import {Directory, Subdirectory} from "./Directory";
import Display from "./Display";
import HistoryDirectory from "./HistoryDirectory";
import PastebinIdeas from "./PastebinIdeas";
import Help from "./Help";
import tumblrs from "./tumblrs";


const $ = window.$;
$(window).blur(store.setter('isFocusLost', true));
$(window).focus(store.setter('isFocusLost', false));

$(window).keypress(function(event) {
  if (event.which === 32) {  // space bar
    store.setter('isRunning', !store.state.isRunning)();
  }
  /*
  } else if (event.keyCode == 39 || event.keyCode == 100) {
    goForward();
  } else if (event.keyCode == 37 || event.keyCode == 97) {
    goBack();
  */
});


class App extends Component {
  constructor() {
    super();
    this.store = store;
    this.state = store.state;
    store.subscribe((newState) => this.setState(newState));
  }

  render() {
    const favoritesCategory = {
      label: "Favorites",
      items: Object.keys(this.state.favorites).sort((a, b) => a < b ? -1 : 1),
    };
    const categories = [favoritesCategory].concat(tumblrs);

    const isHelpVisible = (
      (this.state.shouldShowHelpWhilePaused && !this.state.isRunning)
      || this.state.sources.length < 1
      );

    return (
      <div className="gn-container">
        <table>
          <tbody>
            <tr>
              {this.state.isSidebarOpen && (
                <td className="sidebar-1">
                  <SourceForm {...this.state} />
                </td>
              )}

              {this.state.isTumblrIdeasOpen && <td className="sidebar-2">
                <Directory {...this.state} categories={categories} />
              </td>}

              {this.state.isPastebinIdeasOpen && <td className="sidebar-2">
                <PastebinIdeas />
              </td>}

              {this.state.isTumblrIdeasOpen && this.state.directoryCategoryIndex !== null && (
                <td className="sidebar-3">
                  <Subdirectory category={categories[this.state.directoryCategoryIndex]}
                                favorites={this.state.favorites}
                                sources={this.state.sources} />
                </td>
              )}

              <td className="display">
                <Display {...this.state} />
                {isHelpVisible && <Help />}
                {this.state.isHistoryOpen && <HistoryDirectory history={this.state.history} />}

                {!this.state.isSidebarOpen && (
                  <div className="sidebar-toggle-open" onClick={store.setter("isSidebarOpen", true)}>
                    {">"}
                  </div>
                )}
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}

export default App;
